def measure(calls = 1, &block)
  pre = Time.now
  calls.times { block.call }
  (Time.now - pre) / calls
end
